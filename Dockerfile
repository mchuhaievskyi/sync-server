# Validating Dockerfile
FROM hadolint/hadolint:v1.17.5-6-gbc8bab9-alpine AS lint
COPY Dockerfile .
RUN hadolint Dockerfile

# Building mock-engine
FROM maven:3.8-jdk-11 AS build
WORKDIR /build
COPY pom.xml pom.xml
COPY src /build/src
RUN mvn clean install -DskipTests=true

# Copying resulting JAR
FROM adoptopenjdk/openjdk16:jre-16.0.1_9-alpine as package
WORKDIR /
COPY --from=build /build/target/sync-sever-0.0.1-SNAPSHOT-spring-boot.jar /sync-server.jar
EXPOSE 31415
ENTRYPOINT ["java", "-jar", "/sync-server.jar"]