package com.enforgic.sync.v1.service;

import com.enforgic.sync.v1.exeption.SyncException;
import com.enforgic.sync.v1.model.Player;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class PlayerService {

    private final Map<UUID, Player> userMap = new HashMap<>();

    public List<Player> getPlayers() {
        return userMap.values().stream().collect(Collectors.toUnmodifiableList());
    }

    public Player getPlayer(UUID playerUuid) {

        if (!userMap.containsKey(playerUuid)) {
            throw new SyncException("Can't find player with uuid = " + playerUuid);
        }

        return userMap.get(playerUuid);
    }

    public Player loginPlayer(String playerName, String deviceId) {

        var existingPlayer = userMap.values()
                .stream()
                .filter(p -> p.getDeviceId().equals(deviceId))
                .findAny();

        if (existingPlayer.isPresent()) {
            return existingPlayer.get().setName(playerName);
        }

        var newPlayer = new Player()
                .setName(playerName)
                .setDeviceId(deviceId);

        userMap.put(newPlayer.getUuid(), newPlayer);

        return newPlayer;
    }
}
