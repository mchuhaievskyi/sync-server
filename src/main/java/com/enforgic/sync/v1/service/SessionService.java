package com.enforgic.sync.v1.service;

import com.enforgic.sync.v1.exeption.SyncException;
import com.enforgic.sync.v1.model.Session;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class SessionService {

    private final Map<UUID, Session> sessionMap = new HashMap<>();

    private final PlayerService playerService;

    public SessionService(PlayerService playerService) {
        this.playerService = playerService;
    }

    public List<Session> getSessions() {
        return sessionMap.values().stream().collect(Collectors.toUnmodifiableList());
    }

    public Session getSession(UUID sessionUuid) {

        if (!sessionMap.containsKey(sessionUuid)) {
            throw new SyncException("Can't find session with uuid = " + sessionUuid);
        }

        return sessionMap.get(sessionUuid);
    }

    public Session createSession(UUID creatorUuid, String sessionName) {
        if (sessionMap.values().stream().map(Session::getName).anyMatch(sessionName::equals)) {
            throw new SyncException("Session already exists with name = " + sessionName);
        }

        var newSession = new Session()
                .setName(sessionName);

        var player = playerService.getPlayer(creatorUuid);
        newSession.getPlayerUuids().add(player.getUuid());

        sessionMap.put(newSession.getUuid(), newSession);

        return newSession;
    }

    public Session joinSession(UUID playerUuid, UUID sessionUuid) {
        var player = playerService.getPlayer(playerUuid);
        var session = getSession(sessionUuid);

        session.getPlayerUuids().add(player.getUuid());

        return session;
    }

    public void leaveSession(UUID playerUuid, UUID sessionUuid) {
        var player = playerService.getPlayer(playerUuid);
        var session = getSession(sessionUuid);

        session.getPlayerUuids().remove(player.getUuid());

        if (session.getPlayerUuids().isEmpty()) {
            sessionMap.remove(session.getUuid());
        }
    }
}
