package com.enforgic.sync.v1.service;

import com.enforgic.sync.v1.model.Session;
import com.enforgic.sync.v1.model.State;
import com.enforgic.sync.v1.model.StateEvent;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Service
public class StateService {

    private final Map<Session, State> stateMap = new HashMap<>();

    private final PlayerService playerService;
    private final SessionService sessionService;

    public StateService(PlayerService playerService, SessionService sessionService) {
        this.playerService = playerService;
        this.sessionService = sessionService;
    }

    public State getState(UUID sessionUuid) {
        var session = sessionService.getSession(sessionUuid);

        if (!stateMap.containsKey(session)) {
            stateMap.put(session, new State());
        }

        return stateMap.get(session);
    }

    public State pushEvent(UUID playerUuid, UUID sessionUuid, String eventName, String eventPayload) {

        var state = getState(sessionUuid);

        var newStateEvent = new StateEvent()
                .setName(eventName)
                .setPayload(eventPayload)
                .setActorUuid(playerUuid);

        if (!state.getEvents().isEmpty()) {
            var lastEvent = state.getEvents().get(state.getEvents().size() - 1);
            newStateEvent.setPreviousEventUuid(lastEvent.getUuid());
        }

        state.getEvents().add(newStateEvent);
        state.setChecksum(state.hashCode());

        return state;
    }
}
