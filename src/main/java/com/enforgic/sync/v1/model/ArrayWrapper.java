package com.enforgic.sync.v1.model;

import java.util.List;

public class ArrayWrapper<T> {
    private List<T> values;

    public ArrayWrapper(List<T> values) {
        this.values = values;
    }

    public List<T> getValues() {
        return values;
    }

    public ArrayWrapper<T> setValues(List<T> values) {
        this.values = values;
        return this;
    }
}
