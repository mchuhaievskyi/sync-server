package com.enforgic.sync.v1.model;

import java.util.Objects;
import java.util.UUID;

public class AbstractModel<T extends AbstractModel<T>> implements Identifiable {
    private UUID uuid;

    public AbstractModel() {
        this.uuid = UUID.randomUUID();
    }

    @Override
    public UUID getUuid() {
        return uuid;
    }

    public T setUuid(UUID uuid) {
        this.uuid = uuid;
        return (T) this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AbstractModel that = (AbstractModel) o;
        return Objects.equals(uuid, that.uuid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uuid);
    }
}
