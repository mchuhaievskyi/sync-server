package com.enforgic.sync.v1.model;

public class Player extends AbstractModel<Player> {

    private String name;
    private String deviceId;

    public String getName() {
        return name;
    }

    public Player setName(String name) {
        this.name = name;
        return this;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public Player setDeviceId(String deviceId) {
        this.deviceId = deviceId;
        return this;
    }
}
