package com.enforgic.sync.v1.model;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class Session extends AbstractModel<Session> {

    private String name;
    private Set<UUID> playerUuids = new HashSet<>();

    public Set<UUID> getPlayerUuids() {
        return playerUuids;
    }

    public Session setPlayerUuids(Set<UUID> playerUuids) {
        this.playerUuids = playerUuids;
        return this;
    }

    public String getName() {
        return name;
    }

    public Session setName(String name) {
        this.name = name;
        return this;
    }
}
