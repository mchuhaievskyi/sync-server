package com.enforgic.sync.v1.model;

import java.util.UUID;

public class StateEvent extends AbstractModel<StateEvent> {

    private UUID actorUuid;
    private UUID previousEventUuid;
    private String name;
    private String payload;

    public UUID getActorUuid() {
        return actorUuid;
    }

    public StateEvent setActorUuid(UUID actorUuid) {
        this.actorUuid = actorUuid;
        return this;
    }

    public UUID getPreviousEventUuid() {
        return previousEventUuid;
    }

    public StateEvent setPreviousEventUuid(UUID previousEventUuid) {
        this.previousEventUuid = previousEventUuid;
        return this;
    }

    public String getName() {
        return name;
    }

    public StateEvent setName(String name) {
        this.name = name;
        return this;
    }

    public String getPayload() {
        return payload;
    }

    public StateEvent setPayload(String payload) {
        this.payload = payload;
        return this;
    }
}
