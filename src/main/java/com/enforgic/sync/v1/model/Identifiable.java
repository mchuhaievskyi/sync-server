package com.enforgic.sync.v1.model;

import java.util.UUID;

public interface Identifiable {
    UUID getUuid();
}
