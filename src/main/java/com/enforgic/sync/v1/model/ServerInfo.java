package com.enforgic.sync.v1.model;

public class ServerInfo extends AbstractModel<ServerInfo> {

    private int build;
    private String version;

    public int getBuild() {
        return build;
    }

    public ServerInfo setBuild(int build) {
        this.build = build;
        return this;
    }

    public String getVersion() {
        return version;
    }

    public ServerInfo setVersion(String version) {
        this.version = version;
        return this;
    }
}
