package com.enforgic.sync.v1.model;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class State extends AbstractModel<State> {

    private int checksum;
    private List<StateEvent> events = new LinkedList<>();

    public List<StateEvent> getEvents() {
        return events;
    }

    public State setEvents(List<StateEvent> events) {
        this.events = events;
        return this;
    }

    public int getChecksum() {
        return checksum;
    }

    public State setChecksum(int checksum) {
        this.checksum = checksum;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        State state = (State) o;
        return Objects.equals(events, state.events);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), events);
    }
}
