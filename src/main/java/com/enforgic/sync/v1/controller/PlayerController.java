package com.enforgic.sync.v1.controller;

import com.enforgic.sync.v1.model.ArrayWrapper;
import com.enforgic.sync.v1.model.Player;
import com.enforgic.sync.v1.service.PlayerService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PlayerController {

    private final PlayerService playerService;

    public PlayerController(PlayerService playerService) {
        this.playerService = playerService;
    }

    @GetMapping("/players")
    public ResponseEntity<ArrayWrapper<Player>> getPlayers() {
        var players = playerService.getPlayers();
        return ResponseEntity.ok(new ArrayWrapper<>(players));
    }
}
