package com.enforgic.sync.v1.controller;

import com.enforgic.sync.v1.model.ServerInfo;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class InfoController {

    private final static ServerInfo CURRENT_INFO = new ServerInfo().setVersion("JAM_25_09_21").setBuild(42);

    @GetMapping("/info")
    public ResponseEntity<ServerInfo> getInfo() {
        return ResponseEntity.ok(CURRENT_INFO);
    }
}
