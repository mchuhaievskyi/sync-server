package com.enforgic.sync.v1.controller;

import com.enforgic.sync.v1.exeption.SyncException;
import com.enforgic.sync.v1.model.State;
import com.enforgic.sync.v1.service.StateService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@RestController
public class StateController {

    private final StateService stateService;

    public StateController(StateService stateService) {
        this.stateService = stateService;
    }

    @GetMapping("/sessions/{sessionUuid}/state")
    public ResponseEntity<State> getState(@PathVariable UUID sessionUuid, @RequestParam Optional<Integer> checksum) {

        try {
            var state = stateService.getState(sessionUuid);

            // long-pool
            if (checksum.isPresent()) {
                var startTime = System.currentTimeMillis();
                while (state.getChecksum() == checksum.get()) {

                    if (System.currentTimeMillis() - startTime > TimeUnit.SECONDS.toMillis(30)) {
                        break;
                    }

                    Thread.sleep(1000);
                    state = stateService.getState(sessionUuid);
                }
            }

            return ResponseEntity.ok(state);

        } catch (SyncException ex) {
            return (ResponseEntity) ResponseEntity.badRequest().body(ex.getMessage());
        } catch (Exception ex) {
            throw new IllegalArgumentException(ex);
        }
    }

    @PostMapping("/sessions/{sessionUuid}/state")
    public ResponseEntity<State> pushEvent(@RequestHeader UUID playerUuid,
                                           @PathVariable UUID sessionUuid,
                                           @RequestBody PushEventModel body) {

        try {
            var state = stateService.pushEvent(playerUuid, sessionUuid, body.getName(), body.getPayload());
            return ResponseEntity.ok(state);

        } catch (SyncException ex) {
            return (ResponseEntity) ResponseEntity.badRequest().body(ex.getMessage());
        }
    }

    public static class PushEventModel {
        private String name;
        private String payload;

        public String getName() {
            return name;
        }

        public PushEventModel setName(String name) {
            this.name = name;
            return this;
        }

        public String getPayload() {
            return payload;
        }

        public PushEventModel setPayload(String payload) {
            this.payload = payload;
            return this;
        }
    }
}
