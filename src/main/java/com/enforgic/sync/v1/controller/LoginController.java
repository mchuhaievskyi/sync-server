package com.enforgic.sync.v1.controller;

import com.enforgic.sync.v1.model.Player;
import com.enforgic.sync.v1.service.PlayerService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoginController {

    private final PlayerService playerService;

    public LoginController(PlayerService playerService) {
        this.playerService = playerService;
    }

    @GetMapping("/login")
    public ResponseEntity<Player> loginPlayer(@RequestParam(defaultValue = "") String playerName,
                                              @RequestParam(defaultValue = "") String deviceId) {

        if (playerName.isEmpty() || deviceId.isEmpty()) {
            return (ResponseEntity) ResponseEntity.badRequest().body("playerName or deviceId cannot be empty");
        }

        var player = playerService.loginPlayer(playerName, deviceId);
        return ResponseEntity.ok(player);
    }
}
