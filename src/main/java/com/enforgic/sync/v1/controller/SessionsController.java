package com.enforgic.sync.v1.controller;

import com.enforgic.sync.v1.exeption.SyncException;
import com.enforgic.sync.v1.model.ArrayWrapper;
import com.enforgic.sync.v1.model.Session;
import com.enforgic.sync.v1.service.SessionService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
public class SessionsController {

    private final SessionService sessionService;

    public SessionsController(SessionService sessionService) {
        this.sessionService = sessionService;
    }

    @GetMapping("/sessions")
    public ResponseEntity<ArrayWrapper<Session>> getSessions() {
        return ResponseEntity.ok(new ArrayWrapper<>(sessionService.getSessions()));
    }

    @PostMapping("/sessions")
    public ResponseEntity<Session> createSession(@RequestHeader UUID playerUuid,
                                                 @RequestParam String sessionName) {
        try {
            return ResponseEntity.ok(sessionService.createSession(playerUuid, sessionName));

        } catch (SyncException ex) {
            return (ResponseEntity) ResponseEntity.badRequest().body(ex.getMessage());
        }
    }

    @PostMapping("/sessions/{sessionUuid}")
    public ResponseEntity<Session> joinSession(@RequestHeader UUID playerUuid,
                                               @PathVariable UUID sessionUuid,
                                               @RequestParam(defaultValue = "false") boolean leave) {
        try {
            if (leave) {
                sessionService.leaveSession(playerUuid, sessionUuid);
                return ResponseEntity.ok().build();
            }

            return ResponseEntity.ok(sessionService.joinSession(playerUuid, sessionUuid));

        } catch (SyncException ex) {
            return (ResponseEntity) ResponseEntity.badRequest().body(ex.getMessage());
        }
    }
}
